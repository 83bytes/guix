(use-modules (guix packages)
	     (guix download))

(use-package-modules
 admin
 aspell
 file
 avr
 backup
 base
 bittorrent
 compression
 cups
 curl
 dico
 dictionaries
 direct-connect
 disk
 dns
 emacs
 fonts
 fontutils
 geo
 gettext
 gimp
 gnome
 gnupg
 gnuzilla
 gobby
 gstreamer
 guile
 image
 image-viewers
 imagemagick
 inkscape
 irc
 libreoffice
 linux
 lxde
 mail
 man
 maths
 messaging
 music
 networking
 parallel
 password-utils
 patchutils
 pdf
 photo
 python
 python-crypto
 rsync
 shells
 shellutils
 ssh
 suckless
 terminals
 tex
 tor
 version-control
 video
 vim
 wdiff
 web
 wget
 wm
 xdisorg
 ncurses
 package-management
 xorg)

(packages->manifest
 (list acpi                ; check battery status
       adwaita-icon-theme
       afew                ; notmuch initial tagging script
       alsa-utils          ; alsamixer for volume control
       arandr              ; connecting projector
       asciinema           ; recording/playing asciinema
       aspell
       aspell-dict-en
       avr-toolchain-4.9   ; developing AVR firmware
       bbdb
       bmon                ; network monitor
       borg
       bwm-ng
       transmission (list transmission "gui")
       ncurses
       clementine
       colordiff           ; better diff
       cups                ; printing
       curl                ; swiss army knife of http
       darkhttpd           ; quick web server
       direnv
       dosfstools          ; format FAT filesystems
       emacs
       file
       epiphany             ; alternative web browser
       ffmpeg               ; converting audio and video
       fish
       font-dejavu
       font-gnu-freefont-ttf
       font-gnu-unifont
       font-wqy-zenhei
       fontconfig
       gajim
       gimp                 ; occasional image manipulation
       git (list git "send-email") ; mostly code development
       gnome-maps
       gnu-gettext          ; for emacs po-mode
       gnu-make             ; mostly building code
       gnupg                ; PGP encryption
       gnuplot              ; producing plots
       gobby
       gst-plugins-good
       hexchat
       hicolor-icon-theme
       htop                 ; better 'top'
       icecat               ; web browser
       imagemagick          ; converting/resizing images
       inkscape
       jq
       keepassxc
       libjpeg
       libreoffice
       linuxdcpp
       lxterminal           ; terminal emulator
       magit
       man-pages            ; standard man pages for programming
       mpop                 ; POP client
       nethogs              ; per process network monitor
       nmap                 ; port scanner, mostly for diagnosing server issues
       notmuch
       openssh              ; SSH server and client (mostly use only the client)
       optipng              ; optimizing PNG
       parallel
       pelican
       perl-image-exiftool  ; reading/writing XMP metadata
       pinentry
       python
       python-ghp-import
       python2-pyopenssl
       python2-pyasn1
       redshift             ; so I can get some sleep at night
       rsync
       rxvt-unicode
       scrot
       sxiv                 ; image viewer
       texlive              ; latex
       tor                  ; anonymity on the Internet
       torsocks
       tree
       uim-gtk              ; Tamil input
       unzip                ; ZIP decompression
       vim
       vlc
       weechat
       wget
       xbacklight           ; controlling screen brightness
       xbindkeys
       xclip                ; command line clipboard manipulation
       youtube-dl           ; download from youtube and other media hosts
       zathura
       zathura-djvu
       zathura-pdf-poppler
       zathura-ps
       zip                  ; ZIP compression
       isc-bind (list isc-bind "utils")       ;; bind is name of guile command so we ave different stuff\
       mutt
       stow
       ))
