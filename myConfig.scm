;; This is an operating system configuration template
;; for a "desktop" setup without full-blown desktop
;; environments.

(use-modules (gnu) (gnu system nss))
(use-service-modules desktop)
(use-package-modules wm emacs vim bootloaders certs suckless gnuzilla mate)

(operating-system
  (host-name "sam")
  (timezone "Asia/Kolkata")
  (locale "en_US.utf8")

  ;; Assuming /dev/sdX is the target hard disk, and "my-root"
  ;; is the label of the target root file system.


   (bootloader
	(bootloader-configuration
		(bootloader grub-efi-bootloader)
		(target "/boot/efi")
		(menu-entries (list (menu-entry
		                	(label "Arch Linux")
					(linux "(hd0,gpt2)/boot/vmlinuz-linux")
					(linux-arguments '("root=/dev/sda2"))
					(initrd "(hd0,gpt2)/boot/initramfs-linux.img"))))))

  (file-systems (cons* (file-system
			(device (uuid "488bea5b-6334-4732-aed4-38fc78f7d366"))
                        (title 'uuid)
                        (mount-point "/")
                        (type "ext4"))
		      (file-system
			(title 'device)
			(device "/dev/sda1")
			(mount-point "/boot/efi")
			(type "vfat"))
		       (file-system
			 (title 'uuid)
			 (device (uuid "62625aaf-172b-48f4-a4bd-bb310daf4d10"))
			 (mount-point "/mnt/data2")
			 (type "ext4"))
		       (file-system
			 (title 'uuid)
			 (device (uuid "c4acb0a1-a295-4f73-88ab-43f65988f480"))
			 (mount-point "/mnt/media")
			 (type "ext4"))
		       (file-system
		      	 (title 'uuid)
		      	 (device (uuid "a99019cb-7cfe-4feb-b1e9-e8d691304ff9"))
		      	 (mount-point "/mnt/data")
		      	 (type "ext4"))
                      %base-file-systems))
  (swap-devices (list "/dev/sda8"))
  (users (cons (user-account
                (name "sohom")
                (comment "Sohom Bhattachaarjee")
                (group "sohom")
		(uid 1000)

                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"))
                (home-directory "/home/sohom"))
               %base-user-accounts))
(groups (cons (user-group
		(name "sohom")
		(id 1000))
	      %base-groups))


  ;; Add a bunch of window managers; we can choose one at
  ;; the log-in screen with F1.
  (packages (cons* i3-wm i3status dmenu ;window managers
                   nss-certs                      ;for HTTPS access
		   emacs                          ; emacs!
		   vim
		   caja
		   icecat
                   %base-packages))

  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with Wicd, and more.
  (services %desktop-services)

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
